
//***********
//***TOOLS***
//***********
function handleToolDown(e){
  curX = e.clientX - canvas.offsetLeft-16;
  curY = e.clientY - canvas.offsetTop -2;//height of icon
  flagTool = true;
  isDrawing = true;
  preX = curX;
  preY = curY;
  ctx.shadowBlur = 0;
  if (currentTool == 'eraser'){
    ctx.globalCompositeOperation = 'destination-out';
  }
  else if(currentTool=='neighbor'){
    points = [ ];
    points.push({ x: e.clientX - canvas.offsetLeft-16, y: e.clientY - canvas.offsetTop -2 });
  }
  else if(currentTool=='Gradient'){
    points.push({ 
      x: e.clientX - canvas.offsetLeft-16, 
      y: e.clientY - canvas.offsetTop -2,
      radius: getRandomInt(10, 30),
      opacity: Math.random()
    });
  }
  else if(currentTool=='shadow'){
    ctx.lineWidth = curWidth;
    ctx.lineJoin = ctx.lineCap = 'round';
    ctx.shadowBlur = 10;
    ctx.shadowColor = curColor;
    ctx.moveTo(e.clientX- canvas.offsetLeft-16, e.clientY- canvas.offsetTop -2);
  }
}
function handleToolUp(e){
  
  flagTool = false;
  isDrawing = false;
  preX = -1;
  preY = -1;
  if (currentTool == 'eraser'){
    ctx.globalCompositeOperation = 'source-over';
  }
  else if(currentTool=='neighbor' || currentTool=='Gradient'||currentTool=='shadow'){
    points.length = 0;
  }
  cPush();
}

function handleToolMove(e){
  curX = e.clientX - canvas.offsetLeft -16;
  curY = e.clientY - canvas.offsetTop-2 ;//height of icon
  if (currentTool == 'pencil' && flagTool || currentTool == 'eraser' && flagTool){
    ctx.beginPath();
    ctx.lineWidth = curWidth;
    ctx.strokeStyle = curColor;
    ctx.moveTo(preX, preY);
    ctx.lineTo(curX, curY);
    ctx.stroke();
    preX = curX;
    preY = curY;
  }
  else if(currentTool=='neighbor' && isDrawing && flagTool){
    if (!isDrawing) return;
      //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      points.push({ x: e.clientX - canvas.offsetLeft-16, y: e.clientY - canvas.offsetTop -2 });

      ctx.beginPath();
      ctx.lineWidth = curWidth;
      ctx.strokeStyle = curColor;
      ctx.moveTo(points[points.length - 2].x, points[points.length - 2].y);
      ctx.lineTo(points[points.length - 1].x, points[points.length - 1].y);
      ctx.stroke();
      
      for (var i = 0, len = points.length; i < len; i++) {
        dx = points[i].x - points[points.length-1].x;
        dy = points[i].y - points[points.length-1].y;
        d = dx * dx + dy * dy;

        if (d < 1000) {
          ctx.beginPath();
          ctx.lineWidth =curWidth;
          ctx.strokeStyle = curColor;
          ctx.moveTo( points[points.length-1].x + (dx * 0.2), points[points.length-1].y + (dy * 0.2));
          ctx.lineTo( points[i].x - (dx * 0.2), points[i].y - (dy * 0.2));
          ctx.stroke();
        }
      }
  }
  else if(currentTool=='Gradient' && flagTool){
    if (!isDrawing) return;
    points.push({ 
      x: e.clientX - canvas.offsetLeft-16, 
      y: e.clientY - canvas.offsetTop -2,
      radius: getRandomInt(5, 20),
      opacity: Math.random()
    });
    
    //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    for (var i = 0; i < points.length; i++) {
      ctx.beginPath();
      ctx.fillStyle = curColor;
      ctx.globalAlpha = points[i].opacity;
      ctx.arc(
        points[i].x, points[i].y, points[i].radius, 
        false, Math.PI * 2, false);
      ctx.fill();
    }
  }
  else if(currentTool=='shadow' && flagTool){
      ctx.lineWidth = curWidth;
      ctx.strokeStyle = curColor;
      ctx.lineTo(e.clientX- canvas.offsetLeft-16, e.clientY - canvas.offsetTop -2);
      ctx.stroke();
  }
}
//***********
//***SHAPE***
//***********
function handleShapeDown(e){
  curX = e.clientX - canvas.offsetLeft;
  curY = e.clientY - canvas.offsetTop;

  flagShape = true;

  canvasTmp.style.display = 'block';
  createShape(new Point(curX, curY));
}

function handleShapeUp(e){
  
  flagShape = false;
  if (currentShape == 'curve' && currentShapeObj.currentMode() < 1){
    console.log(currentShapeObj.currentMode());
    return;
  }
  
  currentShapeObj.draw(ctx);
  currentShapeObj = null;
  cPush();
  
}

function handleShapeMove(e){
  curX = e.clientX - canvas.offsetLeft;
  curY = e.clientY - canvas.offsetTop + 18;//height of icon

  if (flagShape){
    ctxTmp.clearRect(0, 0, w, h);
    ctxTmp.beginPath();
    if (currentShape == 'curve')
    {
      currentShapeObj.setMidPoint(new Point(curX,curY));
    }else
      currentShapeObj.setLastpoint(new Point(curX, curY));
    currentShapeObj.draw(ctxTmp);
  }
}

function createShape(point){
  switch (currentShape) {
    case 'line':{
      currentShapeObj = new Line(point,curWidth,curColor);
      break;
    }
    case 'ellipse':{
      currentShapeObj = new Ellipse(point, curWidth, curColor);
      break;
    }
    case 'rectangle':{
      currentShapeObj = new Rectangle(point, curWidth, curColor);
      break;
    }
    case 'equilateral':{
      currentShapeObj = new Equilateral(point, curWidth, curColor);
      break;
    }
    default:
  }
}
function download(){ 
  var canvas=document.querySelector('#canvas'); 
  var ctx=canvas.getContext("2d"); 
  var base64Img = canvas.toDataURL(); 
  var oA = document.createElement('a'); 
  oA.href = base64Img; 
  oA.download = 'your picture.png'; 

  var event = document.createEvent('MouseEvents'); 
  event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null); 
  oA.dispatchEvent(event); 
};
function test1() 
{ 
  fileUpload.click();   
  textfield.value = upload.value;   
}
$(document).ready(function(){
  $("button").click(function(){
      $("#animal").css('z-index',-1);
      $("#test2").css('z-index',-2);
      $("#animal").animate({
          opacity: '0',
      });
      $("#test2").animate({
        opacity: '0',
    });
  });
});
