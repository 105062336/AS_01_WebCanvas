# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* **COVER**<br>
<img src="cover.png" width="460px" height="270px"></img>
* **CONTENT**<br>
<img src="canvas.png" width="460px" height="270px"></img>
* Function of My Canvas
    * <img src="assets/img/tools/pencil.png" width="16px" height="16px"></img>**Pencil**
        * We can choose any thickness to draw on canvas.
    * <img src="assets/img/tools/eraser.png" width="16px" height="16px"></img>**Eraser**
        * We can erase anything on the canvas.
    * <img src="assets/img/tools/clear.png" width="16px" height="16px"></img>**ClearPad**
        * One button that can clear all pad.
    * <img src="assets/img/tools/text.png" width="16px" height="16px"></img>**Text on Canvas**
        * You can choose the typeface on the right side.
        * If you wanna change the font size, you can scroll the thickness to make it bigger.
    * <img src="assets/img/tools/undo.png" width="16px" height="16px"></img>**Undo**
        * You can undo the canvas as long as you make a mistake.
        * **But the extra functions are not included, there are some bugs in the codes.**
    * <img src="assets/img/tools/redo.png" width="16px" height="16px"></img>**Redo**
        * If you regret making undo, you can press it to recover your canvas.
    * <img src="assets/img/tools/download.png" width="16px" height="16px"></img>**Download File**
        * You can download the canvas you have created.
    * <img src="assets/img/tools/upload.png" width="16px" height="16px"></img>**Upload File**
        * You can pick up a file to make it different, beautifying it.
    * <img src="assets/img/shapes/line.png" width="16px" height="16px"></img>**Draw Line**
        * Draw a line.
    * <img src="assets/img/shapes/ellipse.png" width="16px" height="16px"></img>**Draw Circle**
        * Draw a circle.
    * <img src="assets/img/shapes/equilateral.png" width="16px" height="16px">**Draw Triangle**
        * Draw a triangle.
    * <img src="assets/img/shapes/rectangle.png" width="16px" height="16px">**Draw Rectangle**
        * Draw a rectangle.
    * **Pick A Color As You Want**
        * <img src="color.png" ></img>
    * **Thickness**
        * <img src="thickness.png" ></img>
    * **Typeface**
        * <img src="typeface.png" ></img>
* Extra Function of My Canvas
    * <img src="assets/img/tools/neighbor.png" width="16px" height="16px">**Neighbor Connected Pencil**
    * <img src="assets/img/tools/gradient.png" width="16px" height="16px">**Gradient**
    * <img src="assets/img/tools/shadow.png" width="16px" height="16px">**Pencil With Shadow**
    * **URL  (source code) :** http://perfectionkills.com/exploring-canvas-drawing-techniques/
    
## Spec Requirement
- [x] Basic control tools
    * Brush and eraser
    * Color selector
    * Simple menu
- [x] Text input
    * User can type texts on canvas
    * Font menu (typeface and size)
- [x] Cursor icon
    * The image should change according to the currently used tool
- [x] Refresh button
    * Reset canvas
- [x] Different brush shapes
    * Circle
    * Rectangle
    * Triangle
- [x] Un/Redo button
- [x] Image tool
    * User can upload image and paste it
- [x] Download
    * Download current canvas as an image file


